#!/usr/bin/env python
"""
This module provides functions to interact with GitLab's API to fetch a group by name
and retrieve all variables associated with that GitLab group and its subgroups recursively.
It uses environment variables for configuration and command line arguments for input. Supports
outputting to JSON format and provides a dry-run functionality for restoring variables,
including creating missing groups, with an option to commit changes using a flag.
"""

import os
import sys
import gitlab
import json
import argparse

def get_env_variable(var_name, default=None):
    """Retrieve environment variable or return a default value."""
    return os.getenv(var_name, default)

def get_gitlab_client(token, url):
    """Initialize and return a GitLab client using the provided token and URL."""
    return gitlab.Gitlab(url, private_token=token)

def find_or_create_group(client, group_name, parent_id=None, commit=False):
    """Retrieve the group object by name or create it if it does not exist."""
    groups = client.groups.list(search=group_name)
    for group in groups:
        if group.name == group_name and (parent_id is None or group.parent_id == parent_id):
            print(f"Group '{group_name}' already exists.")
            return group

    if commit:
        group_data = {'name': group_name, 'path': group_name.lower(), 'parent_id': parent_id}
        group = client.groups.create(group_data)
        print(f"Group '{group_name}' created.")
        return group
    else:
        print(f"Group '{group_name}' would be created.")
        return None

def restore_variables(client, group_name, data, parent_id=None, commit=False):
    """Restore variables for a group and its subgroups from the given data, creating groups if they don't exist."""
    group = find_or_create_group(client, group_name, parent_id, commit)
    if not group:
        return

    if group.name in data:
        current_vars = {var.key: var for var in group.variables.list()}
        for key, value in data[group.name].items():
            if key in current_vars:
                if current_vars[key].value != value:
                    if commit:
                        current_vars[key].value = value
                        current_vars[key].save()
                        print(f"Variable '{key}' in group '{group_name}' updated.")
                    else:
                        print(f"Variable '{key}' in group '{group_name}' would be updated from '{current_vars[key].value}' to '{value}'.")
                else:
                    print(f"Variable '{key}' in group '{group_name}' remains unchanged.")
            else:
                if commit:
                    group.variables.create({'key': key, 'value': value})
                    print(f"Variable '{key}' added to group '{group_name}'.")
                else:
                    print(f"Variable '{key}' would be added to group '{group_name}'.")

    # Handle subgroups
    if 'subgroups' in data[group.name]:
        for subgroup_name, _ in data[group.name]['subgroups'].items():
            restore_variables(client, subgroup_name, data[group.name]['subgroups'], group.id, commit)

def main():
    """Main function to either retrieve and print or restore group variables based on command line arguments."""
    parser = argparse.ArgumentParser(description='Script to manage GitLab group variables. It can export variables to JSON and restore them from a JSON file or stdin, including creating missing groups.')
    parser.add_argument('group_name', help='Name of the GitLab group to process.')
    parser.add_argument('-r', '--restore', action='store_true', help='Enable restore mode. Reads from stdin by default or from a file if -f is specified.')
    parser.add_argument('-f', '--file', metavar='FILE', help='File to read JSON data from when restoring variables.')
    parser.add_argument('-c', '--commit', action='store_true', help='Commit changes during restore. Without this flag, the script will run in dry-run mode.')

    args = parser.parse_args()

    token = get_env_variable('GITLAB_ACCESS_TOKEN')
    if not token:
        print("Error: The environment variable 'GITLAB_ACCESS_TOKEN' is not set.")
        sys.exit(1)

    gitlab_url = get_env_variable('GITLAB_URL', 'https://gitlab.com')
    client = get_gitlab_client(token, gitlab_url)

    if args.restore:
        if args.file:
            with open(args.file, 'r') as f:
                data = json.load(f)
        else:
            data = json.load
